import axios from 'axios';
import { API_URL } from './config';

const instance = axios.create({
  baseURL: API_URL,
  timeout: 4000,
  headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') }
});

export default instance;