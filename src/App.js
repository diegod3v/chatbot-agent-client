import { ConnectedRouter } from 'connected-react-router';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute';
import configureStore, { history } from './configureStore';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';

const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <>
            <Switch>
              <PrivateRoute path='/app' component={Dashboard} />
              <Route path='/login' component={Login} />
              <Redirect from='/' to='/app' />
            </Switch>
          </>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
