import { createActions } from 'redux-actions';

export const {
  flowConfigReceived,
  getFlowConfig,
  createFlowNode,
  getFlowNode,
  updateFlowNode,
  deleteFlowNode,
  flowNodeReceived,
  setOptionNextNode,
  createNodeOption,
  updateNodeOption,
  deleteNodeOption,
  nodeOptionReceived,
} = createActions(
  'FLOW_CONFIG_RECEIVED',
  'GET_FLOW_CONFIG',
  'CREATE_FLOW_NODE',
  'GET_FLOW_NODE',
  'UPDATE_FLOW_NODE',
  'DELETE_FLOW_NODE',
  'FLOW_NODE_RECEIVED',
  'SET_OPTION_NEXT_NODE',
  'CREATE_NODE_OPTION',
  'UPDATE_NODE_OPTION',
  'DELETE_NODE_OPTION',
  'NODE_OPTION_RECEIVED',
);
