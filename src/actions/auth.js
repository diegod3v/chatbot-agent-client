import { createActions } from 'redux-actions';

export const {
  login,
  logout,
  userInfoReceived,
  getUserInfo
} = createActions(
  'LOGIN',
  'LOGOUT',
  'USER_INFO_RECEIVED',
  'GET_USER_INFO',
);
