import { createActions } from 'redux-actions';

export const { editNode, editOption, getIntents, intentsReceived } = createActions(
  'EDIT_NODE',
  'EDIT_OPTION',
  'GET_INTENTS',
  'INTENTS_RECEIVED',
)