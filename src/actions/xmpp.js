import { createActions } from 'redux-actions';

export const {
  clientConnecting,
  clientConnected,
  connectClient,
  sendMessage,
  agentMessageReceived,
  clientMessageReceived,
  clientMessageObjReceived,
  customerInfoReceived,
} = createActions(
  'CLIENT_CONNECTING',
  'CLIENT_CONNECTED',
  "CONNECT_CLIENT",
  "SEND_MESSAGE",
  "AGENT_MESSAGE_RECEIVED",
  "CLIENT_MESSAGE_RECEIVED",
  "CLIENT_MESSAGE_OBJ_RECEIVED",
  "CUSTOMER_INFO_RECEIVED",
);
