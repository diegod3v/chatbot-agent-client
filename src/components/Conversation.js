import React from 'react'
import styled from 'styled-components';
import { Row as RSRow, Col } from 'reactstrap';
import noAvatarImg from '../noavatar.png';

const ConversationContainer = styled.article`
  color: #fff;
  padding: 0.5rem;
`;

const ConversationName = styled.h5`
  font-size: 0.9rem;
  font-weight: 700;
  margin-bottom: 4px;
`;

const ConversationPreview = styled.p`
  font-size: 0.7rem;
  max-width: 100%;
  max-height: 1rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-bottom: 0;
`;

const ConversationAvatarContainer = styled.div`
    width: 100%;
    padding: 0.5rem;
`;

const ConversationAvatar = styled.img`
    width: 100%;
    border-radius: 5rem;
`;

const Row = styled(RSRow)`
  align-items: center;
`


const Conversation = ({ avatar, name, lastMessage }) => (
  <ConversationContainer>
    <Row noGutters alignItem='center'>
      <Col md='4'>
        <ConversationAvatarContainer>
          <ConversationAvatar src={noAvatarImg} />
        </ConversationAvatarContainer>
      </Col>
      <Col md='8'>
        <ConversationName>{name}</ConversationName>
        <ConversationPreview>{lastMessage}</ConversationPreview>
      </Col>
    </Row>
  </ConversationContainer>
);

export default Conversation;