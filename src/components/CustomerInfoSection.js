import React from 'react';
import { Card, CardBody } from 'reactstrap';
import styled from 'styled-components';
import noAvatarImg from '../noavatar.png';
import { Desktop as ComputerIcon } from 'styled-icons/fa-solid/Desktop';
import { MousePointer as PointerIcon } from 'styled-icons/fa-solid/MousePointer';
import { User as UserIcon } from 'styled-icons/fa-solid/User';
import { Email as EmailIcon } from 'styled-icons/material/Email';
import { Phone as PhoneIcon } from 'styled-icons/material/Phone';

export const CustomerInfoContainer = styled.div`
height: 100%;
background: linear-gradient(to bottom, #1e5799 0%,#ebeef3 100%);
`;

export const CustomerInfoAvatarContainer = styled.div`
width: 100%;
padding: 2rem 4rem;
`;

export const CustomerInfoAvatar = styled.img`
width: 100%;
height: auto;
border-radius: 100%;
box-shadow: 0px 15px 65px -24px rgba(0,0,0,0.76);
`;

export const CustomerInfoCard = styled(Card)`
margin: 2rem;
box-shadow: 0px 15px 65px -24px rgba(0,0,0,0.76);
`;

export const CustomerInfoCardBody = styled(CardBody)``;

export const CustomerInfoList = styled.ul`
list-style: none;
`;

export const InfoIcon = styled.span`
display: block;
height: 1.4rem;
width: 1.4rem;
float: left;
margin-left: -2.5rem;
svg {
  height: 1.4rem;
  width: 1.4rem;
  color: #B1B1B1;
}
`;

export const CustomerInfoItem = styled.li`
margin-top: 1rem;
margin-bottom: 1rem;
font-size: 0.9rem;
`;


const CustomerInfoSection = ({ name, phone, email, lastVisitedUrl, device }) => {
  return (
    <CustomerInfoContainer>
      <CustomerInfoAvatarContainer>
        <CustomerInfoAvatar src={noAvatarImg} />
      </CustomerInfoAvatarContainer>
      <CustomerInfoCard>
        <CustomerInfoCardBody>
          <CustomerInfoList>
            <CustomerInfoItem><InfoIcon><UserIcon /></InfoIcon>{name || 'Sin Datos'}</CustomerInfoItem>
            <CustomerInfoItem><InfoIcon><PhoneIcon /></InfoIcon>{phone || 'Sin Datos'}</CustomerInfoItem>
            <CustomerInfoItem><InfoIcon><EmailIcon /></InfoIcon>{email || 'Sin Datos'}</CustomerInfoItem>
            <CustomerInfoItem><InfoIcon><PointerIcon /></InfoIcon>{lastVisitedUrl || 'Sin Datos'}</CustomerInfoItem>
            <CustomerInfoItem><InfoIcon><ComputerIcon /></InfoIcon>{device || 'Sin Datos'}</CustomerInfoItem>
          </CustomerInfoList>
        </CustomerInfoCardBody>
      </CustomerInfoCard>
    </CustomerInfoContainer>
  )
}

export default CustomerInfoSection;