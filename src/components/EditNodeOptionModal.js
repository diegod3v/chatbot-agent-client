import React, { Component } from 'react';
import { Button, Form, FormGroup, FormText, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';


class EditNodeOptionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props.option,
    }
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen} toggle={this.props.toggle}>
        <ModalHeader toggle={this.props.toggle}>Editar Opción</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="optionText">Texto</Label>
              <Input
                type="text"
                name="optionText"
                id="optionText"
                placeholder="Quiero información del envio"
                value={this.state.text}
                onChange={this.handleInputChange('text')}
              />
              <FormText>
                Es el mensaje que enviará el usuario al dar click al botón
                </FormText>
            </FormGroup>
          </Form>
          {this.state.id && <Button color="danger" onClick={e => this.props.onDelete(this.state.id)}>Borrar</Button>}
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.onSave}>Guardar</Button>{' '}
          <Button color="secondary" onClick={this.props.toggle}>Cancelar</Button>
        </ModalFooter>
      </Modal>
    )
  }

  onSave = () => {
    const nodeOption = { ...this.state };
    this.props.onSave(nodeOption);
  }

  handleInputChange = (value) => (e) => {
    const { value: inputValue } = e.target;
    this.setState({
      [value]: inputValue,
    });
  }
}

export default EditNodeOptionModal;
