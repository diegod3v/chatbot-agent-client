import React, { Component } from 'react';
import { MessageBubble, MessageText } from './Messenger.styled';

export class EmailMessageNodeBody extends Component {
  render() {
    const { payload, type } = this.props;
    const parsedPayload = JSON.parse(payload);
    return (
      <div>
        <MessageBubble bg={'#2cc4f2'}>
          <MessageText>
            {parsedPayload.text}
          </MessageText>
        </MessageBubble>
      </div>
    )
  }
}

export default EmailMessageNodeBody
