import { pick } from 'lodash';
import React, { Component } from 'react';
import { Button, Form, FormGroup, FormText, Input, Label } from 'reactstrap';

export class EmailNodeEditionForm extends Component {
  constructor(props) {
    super(props);
    const nodeProps = pick(props, ['info', 'event']);
    this.state = {
      ...nodeProps,
      info: { ...props.info, payload: JSON.parse(props.info.payload) },
    };
  }

  get payloadText() {
    return this.state.info.payload.text;
  }

  setPayloadText = (e) => {
    const { target: { value } } = e;
    this.setState((prevState) => (
      { info: { ...prevState.info, payload: { ...prevState.info.payload, text: value } } }
    ));
  }

  render() {
    return (
      <div>
        <Form>
          <div>
            <h4>Mensaje</h4>
            <FormGroup>
              <Label for="bubbleText">Mensaje del Correo</Label>
              <Input
                type="textarea"
                name="bubbleText"
                id="bubbleText"
                placeholder="Escribe un mensaje!"
                value={this.payloadText}
                onChange={this.setPayloadText}
              />
            </FormGroup>
          </div>
          <br />
          <br />
          <Button
            type='button'
            color={'primary'}
            onClick={e => this.props.onSave(
              {
                ...this.state,
                info: { ...this.state.info, payload: JSON.stringify(this.state.info.payload) }
              }
            )}
          >
            Aplicar
          </Button>
          <br />
          <Button
            className='mt-4'
            type='button'
            color={'danger'}
            onClick={this.props.onDelete}
          >
            Eliminar Nodo
          </Button>
        </Form>
      </div>
    )
  }
}

export default EmailNodeEditionForm;
