import React, { Component } from 'react'
import { jsPlumb } from 'jsplumb';
import styled from 'styled-components';
import { fromPairs } from 'lodash';

const CONTAINER_ID = "ego__flow-container"

const JPContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

const ENDPOINT_OPTIONS = ["Dot", { radius: 7 }];

class FlowContainer extends Component {
  constructor(props) {
    super(props);
    this.jsPlumb = jsPlumb.getInstance(
      { Container: CONTAINER_ID }
    );
    this.jsPlumb.importDefaults({
      Endpoints: [ENDPOINT_OPTIONS, ENDPOINT_OPTIONS],
      EndpointStyles: [{ fill: '#1A237E' }, { fill: '#1A237E' }],
      Anchors: ['Top', 'RightMiddle'],
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log(nextProps, nextState);
    return true;
  }

  componentDidUpdate(prevProps) {
    const { children: prevChildren } = prevProps;
    const { children } = this.props;
    if (!prevChildren || children && (prevChildren.length !== children.length)) {
      this.setupDraggableChildrens(children);
    } else {
      this.jsPlumb.repaintEverything();
    }
  }

  componentDidMount() {
    const { children } = this.props;
    this.setupDraggableChildrens(children);
    this.jsPlumb.bind('connectionDetached', this.props.onConnectionDetached);
    this.jsPlumb.bind('connectionMoved', this.props.onConnectionMoved)
  }

  componentWillUnmount() {
    this.jsPlumb.unbind('connection');
    this.jsPlumb.unbind('connectionDetached');
    this.jsPlumb.unbind('connectionMoved');
  }

  render() {
    return (
      <JPContainer id={CONTAINER_ID}>
        {this.props.children}
      </JPContainer>
    )
  }

  setupDraggableChildrens(children) {
    this.jsPlumb.unbind('connection');
    if (!children) return;
    this.jsPlumb.ready(() => {
      this.jsPlumb.batch(() => {
        const nodesProps = children.map((child) => Array.isArray(child) ? child.map((el) => el.props) : child.props)
          .reduce((prev, curr) => prev.concat(curr), []);
        const nodesIds = nodesProps.map(props => props.id);

        const draggableElements = document.querySelectorAll('.CBP__draggable-node')
        this.jsPlumb.draggable(draggableElements, {
          containment: true,
          grid: [8, 8]
        });

        const optionsNodes = nodesProps.map((node) => {
          const { options } = node;
          if (options) {
            return options.map((option) => ({ source: option.id, target: option.nextNodeId }))
          }
          return undefined;
        }).reduce((prev, curr) => prev.concat(curr), []);

        const nodesEnpointsPairs = nodesIds.map((target) => {
          const endpoint = this.jsPlumb.addEndpoint(target, { anchor: "Top", endpoint: ENDPOINT_OPTIONS, maxConnections: -1 }, { isTarget: true });
          return [target, endpoint]
        });
        const nodeEnpoints = fromPairs(nodesEnpointsPairs);

        const nodesNextsEnpointsPairs = nodesProps.map((node) => {
          if (node.nextNodeId !== null) {
            const endpoint = this.jsPlumb.addEndpoint(node.id, { anchor: "Bottom", endpoint: ENDPOINT_OPTIONS }, { isSource: true });
            return [node.id, endpoint]
          } else return undefined;
        }).filter(Boolean);
        const nodeNextsEnpoints = fromPairs(nodesNextsEnpointsPairs);

        const optionsEndpointsPairs = optionsNodes.map((option) => {
          const { source } = option;
          const endpoint = this.jsPlumb.addEndpoint(source, { anchor: "RightMiddle", endpoint: ENDPOINT_OPTIONS }, { isSource: true });
          return [source, endpoint]
        });
        const optionEndpoints = fromPairs(optionsEndpointsPairs);

        const connections = optionsNodes
          .filter((connection) => connection && connection.source && connection.target);

        const nodesNextsConnections = nodesProps
          .filter((node) => node && node.nextNodeId);

        connections.map((connection) => {
          const { target, source } = connection;
          return this.jsPlumb.connect({
            source: optionEndpoints[source],
            target: nodeEnpoints[target],
          })
        });

        nodesNextsConnections.map((node) => {
          const { id, nextNodeId } = node;
          return this.jsPlumb.connect({
            source: nodeNextsEnpoints[id],
            target: nodeEnpoints[nextNodeId],
          })
        });

        this.jsPlumb.repaint()
        this.jsPlumb.bind('connection', this.props.onConnection);
      })
    })
  }
}

export default FlowContainer
