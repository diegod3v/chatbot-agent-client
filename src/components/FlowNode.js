import React from 'react'
import styled from 'styled-components';
import { Pencil as PencilIcon } from 'styled-icons/boxicons-solid/Pencil';
import FlowNodeBody from './FlowNodeBody';

const Content = styled.div`
  display: inline-block;
  position: absolute;
  width: ${({ width }) => width ? width : '18rem'};
  height: ${({ height }) => height};
  top: ${({ top }) => `${top || 0}px`};
  left: ${({ left }) => `${left || 0}px`};
`;

const Body = styled.div`
  flex: 1 1 auto;
  padding: 0.25rem;
`;

const IconButton = styled.span`
  display: block;
  position: absolute;
  top: ${({ top }) => `${(top - 14) || 0}px`};
  right: ${({ right }) => `${(right - 14) || 0}px`};
  height: 1.8rem;
  width: 1.8rem;
  line-height: 0;
  padding: 4px;
  border-radius: 100%;
  background: white;
  text-align: center;
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
  border: 1px solid transparent;
  color: #007bff;
  &:hover {
    border: 1px solid #007bff;
  }
`;

const FlowNode = ({ info, options, onEdit, ...rest }) => (
  <Content className='CBP__draggable-node'  {...rest}>
    <IconButton onClick={onEdit} right={10} top={10}><PencilIcon /></IconButton>
    <Body>
      <FlowNodeBody type={info.type} payload={info.payload} options={options} />
    </Body>
  </Content>
)

export default FlowNode;