import React, { Component } from 'react'
import TextMessageNodeBody from './TextMessageNodeBody';
import QuestionMessageNodeBody from './QuestionMessageNodeBody';
import EmailMessageNodeBody from './EmailMessageNodeBody';

export class FlowNodeBody extends Component {
  render() {
    const { type } = this.props;
    if (type === 'text') {
      return (<TextMessageNodeBody {...this.props} />)
    } else if (type === 'question') {
      return (<QuestionMessageNodeBody {...this.props} />)
    } else if (type === 'email') {
      return (<EmailMessageNodeBody {...this.props} />)
    } else {
      return null;
    }
  }
}

export default FlowNodeBody
