import styled from 'styled-components';

export const MessageBubble = styled.div`
  background-color: ${({ bg }) => bg ? bg : '#3578e5'};
  font-size: 0.85rem;
  border-radius: 1.6rem;
  color: #fff;
  box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.28);
`;

export const MessageText = styled.p`
  padding: 0.6rem 0.9rem;
  margin: 0;
`

export const MessageButtonList = styled.ul`
  background-color: #fff;
  margin: 0;
  padding: 0;
  color: #008bff;
  list-style: none;
  border-radius: 0 0 1.6rem 1.6rem;
  text-align: center;
`;

export const MessageButton = styled.li`
  padding: 0.7rem 0.9rem;
  border-bottom: 2px solid #f1f1f1;
  &:last-child {
    border: none;
  }
`;