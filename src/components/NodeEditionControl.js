import React, { Component } from 'react'
import TextNodeEditionForm from './TextNodeEditionForm';
import QuestionNodeEditionForm from './QuestionNodeEditionForm';
import EmailNodeEditionForm from './EmailNodeEditionForm';

export class NodeEditionControl extends Component {
  render() {
    const { type } = this.props.node.info;
    if (type === 'text') {
      return (
        <TextNodeEditionForm
          onSave={this.props.onSave}
          onDelete={this.props.onDelete}
          onAddOption={this.props.onAddOption}
          onEditOption={this.props.onEditOption}
          intents={this.props.intents}
          {...this.props.node}
        />
      )
    } else if (type === 'question') {
      return (
        <QuestionNodeEditionForm
          onSave={this.props.onSave}
          onDelete={this.props.onDelete}
          onAddOption={this.props.onAddOption}
          onEditOption={this.props.onEditOption}
          {...this.props.node}
        />
      )
    } else if (type === 'email') {
      return (
        <EmailNodeEditionForm
          onSave={this.props.onSave}
          onDelete={this.props.onDelete}
          onAddOption={this.props.onAddOption}
          onEditOption={this.props.onEditOption}
          {...this.props.node}
        />
      )
    }
    else {
      return null;
    }
  }
}

export default NodeEditionControl
