import React, { Component } from 'react';
import { MessageBubble, MessageText } from './Messenger.styled';

export class QuestionMessageNodeBody extends Component {
  render() {
    const { payload, type } = this.props;
    const parsedPayload = JSON.parse(payload);
    return (
      <div>
        <MessageBubble bg={'#F29A2C'}>
          <MessageText>
            {parsedPayload.text}
          </MessageText>
        </MessageBubble>
      </div>
    )
  }
}

export default QuestionMessageNodeBody
