import { pick } from 'lodash';
import React, { Component } from 'react';
import { Button, Form, FormGroup, FormText, Input, Label } from 'reactstrap';

export class QuestionNodeEditionForm extends Component {
  constructor(props) {
    super(props);
    const nodeProps = pick(props, ['info', 'event']);
    this.state = {
      ...nodeProps,
      info: { ...props.info, payload: JSON.parse(props.info.payload) },
    };
  }

  get payloadText() {
    return this.state.info.payload.text;
  }

  setPayloadText = (e) => {
    const { target: { value } } = e;
    this.setState((prevState) => (
      { info: { ...prevState.info, payload: { ...prevState.info.payload, text: value } } }
    ));
  }

  get payloadValue() {
    return this.state.info.payload.value;
  }

  setPayloadValue = (e) => {
    const { target: { value } } = e;

    const re = /^[a-zA-Z\-_]+$/g
    if (re.test(value)) {
      this.setState((prevState) => (
        { info: { ...prevState.info, payload: { ...prevState.info.payload, value: value } } }
      ));
    }
  }

  get payloadValueType() {
    return this.state.info.payload.valueType;
  }

  setPayloadValueType = (e) => {
    const { target: { value } } = e;
    this.setState((prevState) => (
      { info: { ...prevState.info, payload: { ...prevState.info.payload, valueType: value } } }
    ));
  }

  render() {
    return (
      <div>
        <Form>
          <div>
            <h4>Mensaje</h4>
            <FormGroup>
              <Label for="bubbleText">Pregunta</Label>
              <Input
                type="textarea"
                name="bubbleText"
                id="bubbleText"
                placeholder="Escribe un mensaje!"
                value={this.payloadText}
                onChange={this.setPayloadText}
              />
            </FormGroup>
            <FormGroup>
              <Label for="payloadValue">Nombre variable</Label>
              <Input
                type="text"
                name="payloadValue"
                id="payloadValue"
                placeholder="nombre_cliente"
                value={this.payloadValue}
                onChange={this.setPayloadValue}
              />
              <FormText color="muted">
                Es un identificador de la respuesta,
                como 'numero_orden' si preguntas por su numero de orden o 'nombre_cliente' si preguntas por su nombre.
              </FormText>
            </FormGroup>
            <FormGroup>
              <Label for="nodeValueType">Tipo de valor esperado</Label>
              <Input
                type="select"
                name="nodeValueType"
                id="nodeValueType"
                value={this.payloadValueType}
                onChange={this.setPayloadValueType}
              >
                {this.props.valueTypes && this.props.valueTypes.map(valueType => (
                  <option value={valueType.value}>{valueType.name}</option>
                ))}
              </Input>
            </FormGroup>
          </div>
          <br />
          <br />
          <Button
            type='button'
            color={'primary'}
            onClick={e => this.props.onSave(
              {
                ...this.state,
                info: { ...this.state.info, payload: JSON.stringify(this.state.info.payload) }
              }
            )}
          >
            Aplicar
          </Button>
          <br />
          <Button
            className='mt-4'
            type='button'
            color={'danger'}
            onClick={this.props.onDelete}
          >
            Eliminar Nodo
          </Button>
        </Form>
      </div>
    )
  }
}

QuestionNodeEditionForm.defaultProps = {
  valueTypes: [
    { value: 'text', name: 'Texto literal' },
    { value: 'contact', name: 'Nombre' },
    { value: 'search_query', name: 'Nombre de producto o termino' },
    { value: 'phone_number', name: 'Numero telefónico' },
    { value: 'email', name: 'Correo electrónico' },
    { value: 'number', name: 'Numero entero' },
  ],
}

export default QuestionNodeEditionForm;
