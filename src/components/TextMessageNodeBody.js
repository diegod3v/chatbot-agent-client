import React, { Component } from 'react'
import { MessageBubble, MessageText, MessageButtonList, MessageButton } from './Messenger.styled';
import { sortBy } from 'lodash';

export class TextMessageNodeBody extends Component {
  render() {
    const { payload, options, type } = this.props;
    const parsedPayload = JSON.parse(payload);
    return (
      <div>
        <MessageBubble type={type}>
          <MessageText>
            {parsedPayload.text}
          </MessageText>
          <MessageButtonList>
            {options && sortBy(options, ['text']).map((option) => {
              return (
                <MessageButton id={option.id} key={option.id}>{option.text}</MessageButton>
              )
            })}
          </MessageButtonList>
        </MessageBubble>
      </div>
    )
  }
}

export default TextMessageNodeBody
