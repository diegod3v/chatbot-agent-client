import React, { Component } from 'react'
import {
  Form, FormGroup, Input, Label, Button, ListGroup, ListGroupItem,
} from 'reactstrap';
import { Plus as PlusIcon } from 'styled-icons/icomoon';
import styled from 'styled-components';
import { pick } from 'lodash';

const RoundButton = styled.button`
  height: 2rem;
  width: 2rem;
  display: block;
  margin: 2rem auto;
  border-radius: 50%;
  &:focus {
    outline: none;
  }
  &:hover {
    background: gray;
  }
`;

export class TextNodeEditionForm extends Component {
  constructor(props) {
    super(props);
    const nodeProps = pick(props, ['options', 'info', 'intent', 'event']);
    this.state = {
      ...nodeProps,
      info: { ...props.info, payload: JSON.parse(props.info.payload) },
    };
  }

  get payloadText() {
    return this.state.info.payload.text;
  }

  setPayloadText = (e) => {
    const { target: { value } } = e;
    this.setState((prevState) => (
      { info: { ...prevState.info, payload: { ...prevState.info.payload, text: value } } }
    ));
  }

  get nodeType() {
    return this.state.info.type;
  }

  setNodeType = (e) => {
    const { target: { value } } = e;
    this.setState((prevState) => (
      { info: { ...prevState.info, type: value } }
    ));
  }

  onChangeCheckbox = (name, defaultValue) => (e) => {
    const { target: { checked } } = e;
    if (checked) {
      this.setState({ [name]: defaultValue })
    } else {
      this.setState({ [name]: undefined })
    }
  }

  onChangeInput = (name) => (e) => {
    const { target: { value } } = e;
    this.setState({ [name]: value })
  }

  render() {
    return (
      <div>
        <Form>
          <FormGroup>
            <Label for="nodeType">Tipo</Label>
            <Input
              type="select"
              name="nodeType"
              id="nodeType"
              value={this.nodeType}
              onChange={this.setNodeType}
            >
              <option value="text">Texto Normal</option>
            </Input>
          </FormGroup>
          <div>
            <h4>Llamada</h4>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" checked={this.state.event} onChange={this.onChangeCheckbox('event', 'site:enter')} />{' '}
                Quiero que este mensaje se envíe cuando mi cliente haga alguna acción en mi sitio
              </Label>
            </FormGroup>
            <FormGroup className={!this.state.event ? 'disabledFormGroup' : ''}>
              <Label for="nodeEvent">Disparar al</Label>
              <Input
                type="select"
                name="nodeEvent"
                id="nodeEvent"
                placeholder="Selecciona el evento"
                value={this.state.event}
                onChange={this.onChangeInput('event')}
                disabled={!this.state.event}
              >
                <option value="site:enter">Entrar al sitio</option>
                <option value="chat:open">Abrir el chat</option>
              </Input>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="checkbox" checked={this.state.intent} onChange={this.onChangeCheckbox('intent', 'shipping_info')} />{' '}
                Quiero que este mensaje se envíe cuando mi cliente escriba una frase fuera de las opciones
              </Label>
            </FormGroup>
            <FormGroup className={!this.state.intent ? 'disabledFormGroup' : ''}>
              <Label for="nodeIntent">Intención</Label>
              <Input
                type="select"
                name="nodeIntent"
                id="nodeIntent"
                placeholder="Selecciona la Intención"
                value={this.state.intent}
                onChange={this.onChangeInput('intent')}
                disabled={!this.state.intent}
              >
                <option defaultValue hidden value={''}>Ninguno</option>
                {this.props.intents && this.props.intents.map(intent => (
                  <option value={intent}>{intent}</option>
                ))}
              </Input>
            </FormGroup>
          </div>
          <div>
            <h4>Mensaje</h4>
            <FormGroup>
              <Label for="bubbleText">Texto</Label>
              <Input
                type="textarea"
                name="bubbleText"
                id="bubbleText"
                placeholder="Escribe un mensaje!"
                value={this.payloadText}
                onChange={this.setPayloadText}
              />
            </FormGroup>
          </div>
          <div>
            <h4>Opciones</h4>

            <ListGroup>
              {
                this.state.options.map(option => {
                  return (
                    <ListGroupItem
                      key={option.id}
                      type="button"
                      tag="button"
                      onClick={e => this.props.onEditOption(option)}
                      action>
                      {option.text}
                    </ListGroupItem>
                  )
                })
              }
            </ListGroup>
            <RoundButton type="button" onClick={this.props.onAddOption}>
              <PlusIcon />
            </RoundButton>
          </div>
          <br />
          <br />
          <Button
            type='button'
            color={'primary'}
            onClick={e => this.props.onSave(
              {
                ...this.state,
                info: { ...this.state.info, payload: JSON.stringify(this.state.info.payload) }
              }
            )}
          >
            Aplicar
          </Button>
          <br />
          <Button
            className='mt-4'
            type='button'
            color={'danger'}
            onClick={this.props.onDelete}
          >
            Eliminar Nodo
          </Button>
        </Form>
      </div>
    )
  }
}

TextNodeEditionForm.defaultProps = {
  intents: [],
}

export default TextNodeEditionForm;
