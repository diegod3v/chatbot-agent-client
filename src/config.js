
export const APP_DOMAIN = process.env.NODE_ENV !== 'production' ? 'localhost' : 'chatbotproj.com';
export const APP_BASENAME = process.env.NODE_ENV !== 'production' ? '/' : '/agent';
export const API_URL = process.env.NODE_ENV !== 'production' ? 'http://localhost:3000/api/' : 'https://chatbotproj.com/app/api/';
export const XMPP_URL = process.env.NODE_ENV !== 'production' ? 'http://localhost:5280/http-bind' : 'https://chatbotproj.com/http-bind';
export const XMPP_AUTH_URL = process.env.NODE_ENV !== 'production' ? 'http://localhost:3000/xmpp/auth' : 'https://chatbotproj.com/app/xmpp/auth';
