import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import { createLogger } from 'redux-logger'
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import persistState from 'redux-localstorage'

import rootReducer from './reducers';
import rootSaga from './sagas';
import { APP_BASENAME } from './config';

export const history = createBrowserHistory({
  basename: APP_BASENAME,
});

const isInProduction = process.env.NODE_ENV === 'production';

const loggerMiddleware = createLogger();
const sagaMiddleware = createSagaMiddleware();
const persistStateConfig = {
  key: 'store',
};

const middlewares = [
  sagaMiddleware,
  thunkMiddleware,
  !isInProduction && loggerMiddleware,
  routerMiddleware(history)
].filter(Boolean);

const enhancers = compose(applyMiddleware(...middlewares), persistState('chat', persistStateConfig));

export default function configureStore(preloadedState) {
  const store = createStore(
    rootReducer(history),
    preloadedState,
    enhancers,
  );

  sagaMiddleware.run(rootSaga);

  return store;
}