import { pick } from 'lodash';
import React, { Component } from 'react';
import { Input, MessageBox } from 'react-chat-elements';
import { connect } from 'react-redux';
import { Button, Col, Row } from 'reactstrap';
import { sendMessage } from '../actions/xmpp';
import Conversation from '../components/Conversation';
import CustomerInfoSection from '../components/CustomerInfoSection';
import { APP_DOMAIN } from '../config';
import { ConversationChatContainer, ConversationChatControls, ConversationChatMessages, ConversationItem, ConversationsContainer, ConversationsList } from './Chat.styled';


class Chat extends Component {
  constructor(props) {
    super(props);
    this.messageInput = React.createRef();
  }
  state = {
    message: '',
    customerId: '',
    customerJid: '',
  }
  render() {
    const { customerId, customerJid } = this.state;
    let customers = Object.values(this.props.customers);;
    let messages = [];
    let customerInfo;
    if (customerId && customerJid) {
      const customer = this.props.customers[this.state.customerId];
      customerInfo = customer;
      const customerMessages = pick(this.props.messages, customer.messages);
      messages = Object.values(customerMessages);
    }

    return (
      <div className='h-100'>
        <Row noGutters className='h-100'>
          <Col md="2">
            <ConversationsContainer>
              <ConversationsList>
                {customers.map((customer) => {
                  return (
                    <ConversationItem onClick={(e) => this.setState({
                      customerId: customer.id,
                      customerJid: `${customer.username}@${APP_DOMAIN}/main`,
                    })}
                      active={customer.id === this.state.customerId}
                    >
                      <Conversation name={customer.name} lastMessage={'Lorem ups==ipsum'} />
                    </ConversationItem>
                  )
                })}
              </ConversationsList>
            </ConversationsContainer>
          </Col>
          <Col md="7">
            {customerJid && customerId &&
              <ConversationChatContainer>
                <ConversationChatMessages>
                  {
                    messages.map((message) => (
                      <MessageBox
                        position={message.isAgent ? 'right' : 'left'}
                        type="text"
                        text={message.text}
                        date={message.date} />
                    ))
                  }
                </ConversationChatMessages>
                <ConversationChatControls>
                  <Input
                    placeholder="Type here..."
                    multiline={true}
                    ref={this.messageInput}
                    onChange={({ target }) => this.setState({ message: target.value })}
                    onKeyPress={(e) => {
                      const { key } = e;
                      if (key === 'Enter') {
                        e.preventDefault();
                        this.props.sendMessage({
                          text: this.state.message,
                          customerJid,
                          customerId,
                        });
                        this.messageInput.current.clear();
                      }
                    }}
                    rightButtons={
                      <Button color="success" onClick={() => {
                        this.props.sendMessage({
                          text: this.state.message,
                          customerJid,
                          customerId,
                        });
                        this.messageInput.current.clear();
                      }}>Enviar</Button>
                    } />
                </ConversationChatControls>
              </ConversationChatContainer>
            }
          </Col>
          <Col md="3">
            {customerId && customerInfo &&
              <CustomerInfoSection {...customerInfo} />
            }
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    customers: state.chat.customers,
  }
}

const mapDispatchToProps = {
  sendMessage,
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
