import styled from 'styled-components';

export const ConversationsContainer = styled.div`
  background-color: #2A3F5B;
  height: 100%;
  width: 100%;
`;

export const ConversationsList = styled.ul`
  list-style: none;
  margin-left: 0;
  padding-left: 0;
  padding-top: 1px;
`;

export const ConversationItem = styled.li`
  background-color: ${({ active }) => active && '#537aaf73'};
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
  cursor: pointer;
`;

export const ConversationChatContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const ConversationChatMessages = styled.div`
  overflow-y: scroll;
  max-height: calc(100vh - 110px);
  flex: 1 1 100%;
  .rce-mbox-text::after {
    display: none;
  }
  .rce-mbox-time {
    display: none;
  }
  .rce-mbox {
    max-width: 65%;
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
  .rce-mbox-right {
    background: #1898EE;
    color: #fff;
    svg {
      fill: #1898EE;
    }
  }
`;

export const ConversationChatControls = styled.div`
  flex: 1 1 3rem;
`;