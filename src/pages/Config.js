import React, { Component } from 'react';
import { Col, Container, Row, Card, CardBody, Form, FormGroup, Label, Input, FormText, Button } from 'reactstrap';
import api from '../ApiAxios';

class Config extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    api.get('/chat/config').then(response => {
      const { data: config } = response;
      this.setState(config);
    });
  }

  render() {
    return (
      <Container className='h-100 py-5'>
        <Row className='h-100'>
          <Col md='12'>
            <Card>
              <CardBody>
                <h1>Configuración</h1>
                <Col md={6}>
                  <Form>
                    <FormGroup>
                      <Label for="botName">Nombre del Robot</Label>
                      <Input type="text" name="botName" id="botName"
                        placeholder="Robocop" value={this.state.botName}
                        onChange={this.valueUpdater('botName')} />
                    </FormGroup>
                    <FormGroup>
                      <Label for="agentName">Nombre del Agente</Label>
                      <Input type="text" name="agentName" id="agentName"
                        placeholder="Pedro Garcia" value={this.state.agentName}
                        onChange={this.valueUpdater('agentName')} />
                    </FormGroup>
                    <FormGroup>
                      <Label for="email">E-mail</Label>
                      <Input type="email" name="email" id="email"
                        placeholder="Robocop" value={this.state.email}
                        onChange={this.valueUpdater('email')} />
                      <FormText color="muted">
                        A este correo llegarán las notificaciones de la plataforma
                    </FormText>
                    </FormGroup>
                  </Form>
                  <Button color='primary' onClick={this.saveConfig}>Guardar</Button>
                </Col>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }

  valueUpdater = (name) => (e) => {
    const value = e.target.value
    this.setState({ [name]: value });
  }

  saveConfig = () => {
    const data = { ...this.state };
    api.post('/chat/config', data).then(response => {
      const { data: config } = response;
      this.setState(config);
    });
  }
}

export default Config;
