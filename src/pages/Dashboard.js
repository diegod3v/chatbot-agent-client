import { NavIcon, NavItem, NavText } from '@trendmicro/react-sidenav';
import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, Navbar, NavbarBrand, UncontrolledDropdown } from 'reactstrap';
import { Bubbles2 as ChatIcon } from 'styled-icons/icomoon/Bubbles2';
import { Cog as CogIcon } from 'styled-icons/icomoon/Cog';
import { Bot as BotIcon } from 'styled-icons/boxicons-solid/Bot';
import { Home as HomeIcon } from 'styled-icons/icomoon/Home';
import Chat from './Chat';
import { Content, Main, SideNav } from './Dashboard.styled';
import { connect } from 'react-redux';
import { getUserInfo, logout } from '../actions/auth';
import { connectClient } from '../actions/xmpp';
import Home from './Home';
import Config from './Config';
import FlowDesigner from './FlowDesigner';

class Dashboard extends Component {
  componentDidMount() {
    this.props.getUserInfo();
    this.props.connectClient();
  }

  render() {
    const { location, history, match } = this.props;
    return (
      <div>
        <SideNav
          onSelect={(selected) => {
            const to = `${match.url}${selected}`;
            if (location.pathname !== to) {
              history.push(to);
            }
          }}
        >
          <SideNav.Toggle />
          <SideNav.Nav defaultSelected="chat">
            <NavItem eventKey="">
              <NavIcon>
                <HomeIcon size={35} />
              </NavIcon>
              <NavText>Inicio</NavText>
            </NavItem>
            <NavItem eventKey="/flow">
              <NavIcon>
                <BotIcon size={35} />
              </NavIcon>
              <NavText>Constructor de Flujo</NavText>
            </NavItem>
            <NavItem eventKey="/chat">
              <NavIcon>
                <ChatIcon size={35} />
              </NavIcon>
              <NavText>Chat</NavText>
            </NavItem>
            <NavItem eventKey="/config">
              <NavIcon>
                <CogIcon size={35} />
              </NavIcon>
              <NavText>Configuración</NavText>
            </NavItem>
          </SideNav.Nav>
        </SideNav>
        <Main>
          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">ChatbotProj</NavbarBrand>
            <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  {this.props.user.username}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={(e) => this.props.logout()}>
                    Log out
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Navbar>
          <Content>
            <Route exact path={`${match.url}`} component={Home} />
            <Route path={`${match.url}/chat`} component={Chat} />
            <Route path={`${match.url}/flow`} component={FlowDesigner} />
            <Route path={`${match.url}/config`} component={Config} />
          </Content>
        </Main>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  }
}

const mapDispatchToProps = {
  getUserInfo,
  logout,
  connectClient,
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
