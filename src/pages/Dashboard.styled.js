import styled from 'styled-components';
import RSSideNav from '@trendmicro/react-sidenav';

export const SideNav = styled(RSSideNav)`
  && {
    background: #0B172B;
  }
  div[class*="sidenav---selected---"] {
    background: #2A3F5B;
  }
`;

export const Main = styled.main`
  margin-left: 64px;
  background-color: #EBEEF3;
`;

export const Content = styled.div`
  height: calc(100vh - 56px );
`;