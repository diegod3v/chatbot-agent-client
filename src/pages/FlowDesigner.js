import { omit } from 'lodash';
import { denormalize } from 'normalizr';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Message as TextMessageIcon } from 'styled-icons/material/Message';
import { Question as QuestionIcon } from 'styled-icons/fa-solid/Question';
import { Email as EmailIcon } from 'styled-icons/material/Email';
import {
  createFlowNode, createNodeOption, getFlowConfig, setOptionNextNode, updateFlowNode,
  updateNodeOption, deleteFlowNode, deleteNodeOption,
} from '../actions/agent';
import { getIntents } from '../actions/designer';
import { editNode, editOption } from '../actions/designer';
import EditNodeOptionModal from '../components/EditNodeOptionModal';
import FlowContainer from '../components/FlowContainer';
import FlowNode from '../components/FlowNode';
import NodeEditionControl from '../components/NodeEditionControl';
import { flowConfig as flowConfigSchema } from '../schemas';
import { getFlowConfig as getFlowConfigSelector } from '../selectors/agents';
import {
  Control, ControlIcon, ControlName, Controls, ExpandedContainer, MainContainer,
  ScrollableAbsoluteContainer, SideControls
} from './FlowDesigner.styled';

const voidNodeOption = {
  text: '',
}

class FlowDesigner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionModal: false,
    }
  }

  componentDidMount() {
    this.props.getIntents();
    this.props.getFlowConfig();
  }

  render() {
    return (
      <>
        <MainContainer>
          <ScrollableAbsoluteContainer>
            <ExpandedContainer>
              <FlowContainer
                onConnection={this.createFlowConnection}
                onConnectionDetached={this.removeFlowConnection}
                onConnectionMoved={this.moveFlowConnection}
              >
                {this.props.flowNodes &&
                  this.props.flowNodes.map((flowNode) => {
                    const { position, info, options } = flowNode;
                    return (
                      <FlowNode
                        id={flowNode.id}
                        key={flowNode.id}
                        nextNodeId={flowNode.nextNodeId}
                        top={position.top}
                        left={position.left}
                        info={info}
                        options={options}
                        onMouseUp={e => {
                          let top = e.currentTarget.style.top || '0';
                          let left = e.currentTarget.style.left || '0';
                          top = parseInt(top.replace('px', ''))
                          left = parseInt(left.replace('px', ''))
                          const nextFlowNode = { position: { top, left } }
                          this.updateNode(flowNode.id, nextFlowNode);
                        }}
                        onEdit={e => this.props.editNode(flowNode)} />

                    )
                  })}
              </FlowContainer>
            </ExpandedContainer>
            <Controls>
              <Control onClick={this.createTextFlowNode}>
                <ControlIcon><TextMessageIcon size={45} /></ControlIcon>
                <ControlName>Mensaje de Texto</ControlName>
              </Control>
              <Control onClick={this.createQuestionFlowNode}>
                <ControlIcon><QuestionIcon size={45} /></ControlIcon>
                <ControlName>Pregunta</ControlName>
              </Control>
              <Control onClick={this.createMailFlowNode}>
                <ControlIcon><EmailIcon size={45} /></ControlIcon>
                <ControlName>Email</ControlName>
              </Control>
            </Controls>
            <SideControls>
              {
                this.props.editingNode ?
                  <NodeEditionControl
                    onSave={editedNode => this.updateNode(this.props.editingNode.id, editedNode)}
                    onDelete={() => this.handleDeleteNodeEditing(this.props.editingNode.id)}
                    onAddOption={this.openCreateOptionModal}
                    onEditOption={this.openEditOptionModal}
                    node={this.props.editingNode}
                    intents={this.props.intents}
                    key={this.props.editingNode.id}
                  />
                  :
                  <h4>Selecciona un nodo para editar</h4>
              }
            </SideControls>
          </ScrollableAbsoluteContainer>
        </MainContainer>
        <EditNodeOptionModal
          isOpen={this.state.optionModal}
          toggle={this.toggleOptionModal}
          onSave={this.handleSaveOptionEditing}
          onDelete={this.handleDeleteOptionEditing}
          option={this.props.editingOption}
          key={this.props.editingOption.id}
        />
      </>
    )
  }

  toggleOptionModal = () => {
    this.setState(prevState => ({
      optionModal: !prevState.optionModal
    }));
  }

  openEditOptionModal = (option) => {
    this.props.editOption({ ...option })
    this.setState({
      optionModal: true,
    });
  }

  openCreateOptionModal = () => {
    this.props.editOption({ ...voidNodeOption });
    this.setState({
      optionModal: true,
    });
  }

  createFlowConnection = ({ sourceId, targetId }) => {
    console.log(sourceId, targetId);
    if (Object.keys(this.props.optionsIds).includes(sourceId)) {
      this.props.setOptionNextNode({ optionId: sourceId, nextNodeId: targetId });
    } else {
      this.props.updateFlowNode({ nodeId: sourceId, nextNode: { nextNodeId: targetId } });
    }
  }

  moveFlowConnection = ({ originalSourceId, newSourceId, originalTargetId, newTargetId }) => {
    console.log('originalSourceId:', originalSourceId, 'newTargetId', newTargetId);

    if (originalSourceId !== newSourceId) {
      this.removeFlowConnection({ sourceId: originalSourceId });
      this.props.setOptionNextNode({ optionId: newSourceId, nextNodeId: newTargetId })
    } else {
      this.props.setOptionNextNode({ optionId: originalSourceId, nextNodeId: newTargetId });
    }
  }

  removeFlowConnection = ({ sourceId }) => {
    console.log('removed sourceId:', sourceId);
    this.props.setOptionNextNode({ optionId: sourceId, nextNodeId: '' });
  }

  createTextFlowNode = () => {
    this.props.createFlowNode({
      position: {
        top: 150,
        left: 200,
      },
      info: {
        type: 'text',
        payload: '{"text": "Hola"}',
      }
    })
  }

  createQuestionFlowNode = () => {
    this.props.createFlowNode({
      nextNodeId: '',
      position: {
        top: 150,
        left: 200,
      },
      info: {
        type: 'question',
        payload: '{"text": "¿Cual es tu nombre?",'
          + '"value": "cliente",'
          + '"valueType": "contact"}',
      }
    })
  }

  createMailFlowNode = () => {
    this.props.createFlowNode({
      nextNodeId: '',
      position: {
        top: 150,
        left: 200,
      },
      info: {
        type: 'email',
        payload: '{"text": "Mensaje de: {{nombre}}"}',
      }
    })
  }

  updateNode = (nodeId, nextNode) => {
    console.log(nextNode);
    this.props.updateFlowNode({ nodeId, nextNode })
  }

  resetEditing = () => {
    this.props.editOption({ ...voidNodeOption })
    this.props.editNode(undefined);
    this.setState({
      optionModal: false,
    });
  }

  handleDeleteNodeEditing = (id) => {
    this.props.deleteFlowNode(id);
    this.resetEditing();
  }

  handleDeleteOptionEditing = (id) => {
    this.props.deleteNodeOption(id);
    this.resetEditing();
  }

  handleSaveOptionEditing = (nodeOption) => {
    const nextOption = { ...nodeOption }
    console.log(nextOption);
    if (nextOption.id) {
      this.props.updateNodeOption({ optionId: nextOption.id, nextOption: omit(nextOption, ['id']) });
    } else {
      this.props.createNodeOption({ ...nextOption, flowNode: this.props.editingNode.id })
    }
    this.resetEditing();
  }
}

const mapStateToProps = (state) => ({
  flowNodes: denormalize(getFlowConfigSelector(state), flowConfigSchema, state.agent).nodes,
  optionsIds: state.agent.options,
  editingNode: state.designer.editingNode,
  editingOption: state.designer.editingOption,
  intents: state.designer.intents,
});

const mapDispatchToProps = {
  getFlowConfig,
  createFlowNode,
  setOptionNextNode,
  updateFlowNode,
  editNode,
  editOption,
  createNodeOption,
  updateNodeOption,
  deleteFlowNode,
  deleteNodeOption,
  getIntents,
};

export default connect(mapStateToProps, mapDispatchToProps)(FlowDesigner);