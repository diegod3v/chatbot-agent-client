import styled from 'styled-components';
import { Button } from 'reactstrap';

export const MainContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;

export const FloatButton = styled(Button)`
  position: absolute;
  left: 2rem;
  top: 2rem;
  z-index: 100;
`;

export const ScrollableAbsoluteContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0;
  bottom: 0;
  overflow: scroll;
`;

export const ExpandedContainer = styled.div`
  position: relative;
  height: 2000px;
  width: 2000px;
`;

export const SideControls = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  padding: 2rem;
  top: 56px;
  right: 0;
  bottom: 0;
  width: 23rem;
  background-color: #fff;
  overflow: scroll;
`;

export const Controls = styled.div`
  position: fixed;
  display: flex;
  padding-right: 4rem;
  padding-left: 4rem;
  bottom: 0;
  left: 64px;
  right: 0;
  height: 6rem;
  max-width: calc(100% - 64px - 23rem);
  background-color: rgba(0, 0, 0, 0.3);
  color: #fff;
`;

export const Control = styled.div`
  width: 8rem;
  height: 6rem;
  padding: 0.5rem;
  text-align: center;
  cursor: pointer;
`;

export const ControlName = styled.span`
  font-size: 0.8rem;
`;

export const ControlIcon = styled.div`
  width: 3.5rem;
  margin: auto;
  margin-bottom: 5px;
`;