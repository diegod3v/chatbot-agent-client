import React, { Component } from 'react';
import { Col, Container, Row, Card, CardBody } from 'reactstrap';


class Home extends Component {
  render() {
    return (
      <Container className='h-100 py-5'>
        <Row className='h-100'>
          <Col md='12'>
            <Card>
              <CardBody>
                <h1>Bienvenido</h1>
                <p>Este es tu panel de Administración. A continuación, veras un menú del lado izquierdo,
                   podrás ver y controlar tu chatbot y chatear con tus clientes. </p>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Home
