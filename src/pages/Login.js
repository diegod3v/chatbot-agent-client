import React, { Component } from 'react';
import { Button, Card, CardBody, Form, FormGroup, Input, Label, Container, Row, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import { connect } from 'react-redux';
import { login } from '../actions/auth';

export class Login extends Component {
  state = {
    shop: '',
    username: '',
    password: '',
  }
  render() {
    return (
      <div>
        <Container>
          <Row className='mt-5'>
            <Col md={{ size: 6, offset: 3 }}>
              <Card>
                <CardBody>
                  <Form onSubmit={(e) => {
                    e.preventDefault();
                    const data = { ...this.state };
                    data.shop = `${data.shop}.myshopify.com`
                    this.props.login({ ...this.state })
                  }}>
                    <FormGroup>
                      <Label for="shop">Shop</Label>
                      <InputGroup>
                        <Input
                          type="text"
                          name="shop"
                          id="shop"
                          placeholder="mystore"
                          onChange={({ target: el }) => this.setState({ [el.name]: el.value })}
                          value={this.state.shop}
                        />
                        <InputGroupAddon addonType="append">.myshopify.com</InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label for="username">Username</Label>
                      <Input
                        type="text"
                        name="username"
                        id="username"
                        placeholder="pedropablo"
                        onChange={({ target: el }) => this.setState({ [el.name]: el.value })}
                        value={this.state.username}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="password">Password</Label>
                      <Input type="password"
                        name="password"
                        id="password"
                        placeholder="supersecret"
                        onChange={({ target: el }) => this.setState({ [el.name]: el.value })}
                        value={this.state.password}
                      />
                    </FormGroup>
                    <Button color='primary'>Sign In</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = {
  login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
