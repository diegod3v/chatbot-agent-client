import { handleActions } from 'redux-actions';
import { flowConfigReceived, flowNodeReceived, nodeOptionReceived } from '../actions/agent';
import { uniq } from 'lodash';

const reducer = handleActions({
  [flowConfigReceived]: (state, { payload }) => (
    {
      ...state,
      flowConfig: { ...payload.result },
      ...payload.entities
    }
  ),
  [flowNodeReceived]: (state, { payload }) => (
    {
      ...state,
      flowConfig: {
        ...state.flowConfig,
        nodes: uniq([...state.flowConfig.nodes, payload.result]),
      },
      nodes: { ...state.nodes, ...payload.entities.nodes },
      positions: { ...state.positions, ...payload.entities.positions },
      infos: { ...state.infos, ...payload.entities.infos },
      options: { ...state.options, ...payload.entities.options },
    }
  ),
  [nodeOptionReceived]: (state, { payload }) => (
    {
      ...state,
      options: { ...state.options, ...payload }
    }
  ),
},
  {
    flowConfig: {
      nodes: []
    },
    nodes: {},
    positions: {},
    infos: {},
    options: {},
  }
)

export default reducer;