import { handleAction } from 'redux-actions';
import { userInfoReceived } from '../actions/auth';

const reducer = handleAction(
  userInfoReceived,
  (state, action) => {
    return {
      user: action.payload
    }
  },
  { user: {} }
);

export default reducer;