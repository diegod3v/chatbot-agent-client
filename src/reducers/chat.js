import { handleActions } from 'redux-actions';
import { agentMessageReceived, clientMessageReceived, customerInfoReceived } from '../actions/xmpp';

const reducer = handleActions({
  [clientMessageReceived]: {
    next(state, { payload }) {
      return {
        ...state,
        messages: { ...state.messages, ...payload }
      }
    },
    throw(state, action) {
      return state;
    }
  },

  [agentMessageReceived]: {
    next(state, { payload }) {
      return {
        ...state,
        messages: { ...state.messages, ...payload }
      }
    },
    throw(state, action) {
      return state;
    }
  },

  [customerInfoReceived]: {
    next(state, { payload }) {
      return {
        ...state,
        customers: { ...state.customers, ...payload }
      }
    },
    throw(state, action) {
      return state;
    }
  },
}, {
    customers: {},
    messages: {},
  })

export default reducer;