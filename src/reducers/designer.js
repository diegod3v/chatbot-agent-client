import { handleActions } from 'redux-actions';
import { editNode, editOption, intentsReceived } from '../actions/designer';

const reducer = handleActions({
  [editNode]: (state, { payload }) => (
    {
      ...state,
      editingNode: payload,
    }
  ),
  [editOption]: (state, { payload }) => (
    {
      ...state,
      editingOption: payload,
    }
  ),
  [intentsReceived]: (state, { payload }) => (
    {
      ...state,
      intents: payload,
    }
  ),
},
  {
    editingOption: {
      text: '',
    }
  }
)

export default reducer;