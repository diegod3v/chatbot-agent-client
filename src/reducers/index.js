import { combineReducers } from 'redux';
import authReducer from './auth';
import chatReducer from './chat';
import agentReducer from './agent';
import designerReducer from './designer';
import { connectRouter } from 'connected-react-router';

const rootReducer = (history) => combineReducers({
  designer: designerReducer,
  auth: authReducer,
  chat: chatReducer,
  agent: agentReducer,
  router: connectRouter(history)
});

export default rootReducer;