import { $msg, Strophe } from 'strophe.js';

export class ChatClient {
  static instance;

  constructor(clientUrl, authUrl, domain) {
    if (this.instance) {
      return this.instance;
    }

    this.client = new Strophe.Connection(clientUrl);
    this.clientUrl = clientUrl;
    this.domain = domain;
    this.authUrl = authUrl;

    this.instance = this;
  }

  async start() {

    const creds = await this.getCreds();

    try {
      this.client.connect(`${creds.username}@${this.domain}/main`, creds.password, (status) => {
        if (status === Strophe.Status.CONNECTED) {
          console.log('connected');
        } else if (status === Strophe.Status.DISCONNECTED) {
          console.log('disconnected');
        }
      });
    } catch (err) {
      console.log(err);
    }
  }

  async getCreds() {
    const username = localStorage.getItem('chatbotproj_username');
    const password = localStorage.getItem('chatbotproj_token');

    return { username, password }
  }

  onMessage(callback) {
    this.client.addHandler((message) => {
      const type = message.getAttribute('type');
      const from = message.getAttribute('from');
      const elems = message.getElementsByTagName('body');
      if (type === "chat" && elems.length > 0) {
        const body = elems[0];
        const bodyText = Strophe.getText(body);
        const objMessage = JSON.parse(bodyText.replace(/&quot;/g, '"'));
        objMessage.from = from;
        callback(objMessage);
      }
      return true;
    }, null, 'message', null, null, null);
  }

  send(stanza) {
    return new Promise((res, rej) => {
      try {
        this.client.send(stanza);
        res(stanza.nodeTree)
      } catch (err) {
        rej(err);
      }
    })
  }

  sendMessage({ text, to }) {
    const stanza = $msg({ to, type: 'chat' })
      .c('body').t(text)

    return this.send(stanza).then(stanzaNode => {
      const type = stanzaNode.getAttribute('type');
      const elems = stanzaNode.getElementsByTagName('body');
      if (type === "chat" && elems.length > 0) {
        const body = elems[0];
        return Strophe.getText(body);
      }
    });
  }

}