import { takeLatest, put, all, } from 'redux-saga/effects';
import api from '../ApiAxios';
import {
  getFlowConfig as getFlowConfigAction,
  flowConfigReceived,
  createFlowNode as createFlowNodeAction,
  getFlowNode as getFlowNodeAction,
  flowNodeReceived,
  setOptionNextNode as setOptionNextNodeAction,
  createNodeOption as createNodeOptionAction,
  updateNodeOption as updateNodeOptionAction,
  nodeOptionReceived,
  updateFlowNode as updateFlowNodeAction,
  deleteFlowNode as deleteFlowNodeAction,
  deleteNodeOption as deleteNodeOptionAction,
} from '../actions/agent';
import { normalize } from 'normalizr';
import { flowConfig as flowConfigSchema, option as optionSchema, node as nodeSchema } from '../schemas';


function* getFlowConfig() {
  try {
    const { data: userFlowConfigData } = yield api.get('/agent/flow');
    const normalizedData = normalize(userFlowConfigData, flowConfigSchema);
    yield put(flowConfigReceived(normalizedData));
  } catch (err) {
    yield put(flowConfigReceived(err));
  }
}

function* createFlowNode(action) {
  try {
    const { data: flowNode } = yield api.post('/agent/flow/node', action.payload);
    const normalizedFlowNode = normalize(flowNode, nodeSchema);
    yield put(flowNodeReceived(normalizedFlowNode));
  } catch (err) {
    yield put(flowNodeReceived(err));
  }
}

function* getFlowNode(action) {
  const { nodeId } = action.payload;
  try {
    const { data: flowNode } = yield api.get(`/agent/flow/node/${nodeId}`);
    const normalizedFlowNode = normalize(flowNode, nodeSchema);
    yield put(flowNodeReceived(normalizedFlowNode));
  } catch (err) {
    yield put(flowNodeReceived(err));
  }
}

function* updateFlowNode(action) {
  const { nextNode, nodeId } = action.payload;
  try {
    const { data: updatedFlowNode } = yield api.patch(`/agent/flow/node/${nodeId}`, nextNode);
    const normalizedFlowNode = normalize(updatedFlowNode, nodeSchema)
    yield put(flowNodeReceived(normalizedFlowNode));
  } catch (err) {
    yield put(flowNodeReceived(err));
  }
}

function* deleteFlowNode(action) {
  const nodeId = action.payload;
  try {
    const { data: nextFlowConfig } = yield api.delete(`/agent/flow/node/${nodeId}`);
    const normalizedData = normalize(nextFlowConfig, flowConfigSchema);
    yield put(flowConfigReceived(normalizedData));
  } catch (err) {
    yield put(flowConfigReceived(err));
  }
}

function* createNodeOption(action) {
  try {
    const { data: nodeOption } = yield api.post('/agent/flow/option', action.payload);
    const normalizedNodeOption = normalize(nodeOption, optionSchema);
    yield put(nodeOptionReceived(normalizedNodeOption));
    yield put(getFlowNodeAction({ nodeId: nodeOption.flowNode.id }))
  } catch (err) {
    yield put(nodeOptionReceived(err));
  }
}

function* updateNodeOption(action) {
  const { nextOption, optionId } = action.payload;
  try {
    const { data: updatedOption } = yield api.patch(`/agent/flow/option/${optionId}`, nextOption);
    const normalizedNodeOption = normalize(updatedOption, optionSchema);
    yield put(nodeOptionReceived(normalizedNodeOption));
    yield put(getFlowNodeAction({ nodeId: updatedOption.flowNode.id }))
  } catch (err) {
    yield put(nodeOptionReceived(err));
  }
}

function* deleteNodeOption(action) {
  const optionId = action.payload;
  try {
    const { data: nextFlowNode } = yield api.delete(`/agent/flow/option/${optionId}`);
    const normalizedNodeOption = normalize(nextFlowNode, nodeSchema)
    yield put(flowNodeReceived(normalizedNodeOption));
  } catch (err) {
    yield put(flowNodeReceived(err));
  }
}

function* setOptionNextNode(action) {
  const { optionId, nextNodeId } = action.payload;

  try {
    const { data: updatedOption } = yield api.patch(`/agent/flow/option/${optionId}`, { nextNodeId });
    const normalizedOption = normalize(updatedOption, optionSchema)
    yield put(nodeOptionReceived(normalizedOption.entities.options));
  } catch (err) {
    yield put(nodeOptionReceived(err));
  }
}

function* watchDeleteFlowNode() {
  yield takeLatest(deleteFlowNodeAction, deleteFlowNode)
}

function* watchDeleteNodeOption() {
  yield takeLatest(deleteNodeOptionAction, deleteNodeOption)
}

function* watchCreateNodeOption() {
  yield takeLatest(createNodeOptionAction, createNodeOption)
}

function* watchUpdateNodeOption() {
  yield takeLatest(updateNodeOptionAction, updateNodeOption)
}

function* watchGetFlowConfig() {
  yield takeLatest(getFlowConfigAction, getFlowConfig);
}

function* watchCreateFlowNode() {
  yield takeLatest(createFlowNodeAction, createFlowNode);
}

function* watchGetFlowNode() {
  yield takeLatest(getFlowNodeAction, getFlowNode);
}

function* watchUpdateFlowNode() {
  yield takeLatest(updateFlowNodeAction, updateFlowNode);
}

function* watchSetOptionNextNode() {
  yield takeLatest(setOptionNextNodeAction, setOptionNextNode);
}

export default function* agentSaga() {
  yield all([
    watchGetFlowConfig(),
    watchCreateFlowNode(),
    watchGetFlowNode(),
    watchUpdateFlowNode(),
    watchDeleteFlowNode(),
    watchSetOptionNextNode(),
    watchCreateNodeOption(),
    watchUpdateNodeOption(),
    watchDeleteNodeOption(),
  ]);
}



