import { takeLatest, put, select, all } from 'redux-saga/effects';
import {
  login as loginAction,
  userInfoReceived,
  getUserInfo as getUserInfoAction,
  logout as logoutAction
} from '../actions/auth';
import { push } from 'connected-react-router';
import api from '../ApiAxios';
import { getUser } from '../selectors/user';

function* login(action) {
  const { payload } = action;
  const { shop: shopName } = payload;
  const shop = shopName.includes('.myshopify.com') ? shopName : `${shopName}.myshopify.com`
  const loginCreds = { ...payload, shop }
  try {
    const { data: tokenData } = yield api.post('/agent/login', loginCreds);
    const { token } = tokenData;
    yield window.localStorage.setItem('token', token);
    yield window.localStorage.setItem('chatbotproj_username', `${payload.username}#${shopName}`);
    yield window.localStorage.setItem('chatbotproj_token', token);
    yield put(getUserInfoAction);
    yield put(push('/'));
  } catch (err) {
    console.log(err);
  }
  yield;
}

function* logout() {
  yield window.localStorage.removeItem('token');
  yield put(push('/'));
}

function* getUserInfo(action) {
  const storedUser = yield select(getUser);
  if (!storedUser.username) {
    try {
      const { data: user } = yield api.get('/agent/me');
      yield put(userInfoReceived(user));
    } catch (err) {
      yield put(userInfoReceived(err));
    }
  }
}

function* watchGetUserInfo() {
  yield takeLatest(getUserInfoAction, getUserInfo);
}

function* watchLogout() {
  yield takeLatest(logoutAction, logout);
}

function* watchLogin() {
  yield takeLatest(loginAction, login);
}

export default function* authSaga() {
  yield all([
    watchLogin(),
    watchLogout(),
    watchGetUserInfo()
  ]);
}



