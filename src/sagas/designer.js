import { takeLatest, put, select, all } from 'redux-saga/effects';
import { getIntents as getIntentsAction, intentsReceived } from '../actions/designer';
import api from '../ApiAxios';

function* getIntents() {
  try {
    const { data: dataIntents } = yield api.get('/agent/intents');
    yield put(intentsReceived(dataIntents.intents));
  } catch (err) {
    yield put(intentsReceived(err));
  }
}

function* watchGetIntents() {
  yield takeLatest(getIntentsAction, getIntents);
}

export default function* designerSaga() {
  yield all([
    watchGetIntents()
  ]);
}



