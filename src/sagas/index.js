import { all } from 'redux-saga/effects';
import authSaga from './auth';
import xmppSaga from './xmpp';
import agentSaga from './agent';
import designerSaga from './designer';

export default function* rootSaga() {
  yield all([
    authSaga(),
    xmppSaga(),
    agentSaga(),
    designerSaga(),
  ])
}