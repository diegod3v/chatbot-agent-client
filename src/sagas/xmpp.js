import { eventChannel } from 'redux-saga';
import { all, call, fork, put, take, takeLatest, select } from 'redux-saga/effects';
import {
  agentMessageReceived, clientConnected, clientConnecting,
  clientMessageReceived, clientMessageObjReceived as clientMessageObjReceivedAction,
  connectClient as connectClientAction, sendMessage as sendMessageAction, customerInfoReceived,
} from '../actions/xmpp';
import api from '../ApiAxios';
import uuidv1 from 'uuid/v1';
import { keyBy, findKey } from 'lodash';
import { ChatClient } from './ChatClient';
import { getCustomers, getCustomer } from '../selectors/xmpp';
import { APP_DOMAIN, XMPP_URL, XMPP_AUTH_URL } from '../config';

const xmppUri = XMPP_URL
const authUri = XMPP_AUTH_URL;
const domain = APP_DOMAIN;

let chatbotProjClient;

function onMessageChannel() {
  return eventChannel(emitter => {
    chatbotProjClient.onMessage(objMessage => {
      try {
        emitter(clientMessageObjReceivedAction(objMessage));
      } catch (err) {
        console.log('error in chanel');
      }
    });

    return () => { }
  })
}

function* onMessageSaga() {
  const channel = yield call(onMessageChannel);
  try {
    while (true) {
      try {
        let messageAction = yield take(channel);
        yield put(messageAction);
      } catch (err) {
        console.log('Error sending message: ', err);
      }
    }
  } finally {
    console.log('messages channel terminated')
  }
}

function* clientMessageObjReceived(action) {
  const { payload: messageObj } = action;
  const { from: customerJid } = messageObj;
  const customerUsername = customerJid.split('@', 2)[0];
  const customers = yield select(getCustomers);

  const messageInfo = {
    ...messageObj,
    uuid: uuidv1(),
    date: new Date(),
  }

  const customerKey = findKey(customers, ['username', customerUsername]);
  const customer = customers[customerKey];

  if (!customer) {
    const { data: customerInfo } = yield api.get('/agent/customer', {
      params: {
        username: customerUsername
      }
    })
    customerInfo.messages = [messageInfo.uuid];
    const customerPayload = keyBy([customerInfo], 'id');
    yield put(customerInfoReceived(customerPayload));
  } else {
    customer.messages = [...customer.messages, messageInfo.uuid];
    const customerPayload = keyBy([customer], 'id');
    yield put(customerInfoReceived(customerPayload));
  }

  const messageObjPayload = keyBy([messageInfo], 'uuid');

  yield put(clientMessageReceived(messageObjPayload));
}

function* connectClient(action) {
  chatbotProjClient = new ChatClient(xmppUri, authUri, domain);

  yield put(clientConnecting);

  yield fork(onMessageSaga);

  try {
    const jid = yield chatbotProjClient.start();
    yield put(clientConnected({ jid }));
  } catch (err) {
    yield put(clientConnected(err));
  }

  yield;
}

function* sendMessage({ payload }) {
  try {
    const messageText = JSON.stringify({ text: payload.text })
    yield chatbotProjClient.sendMessage({ text: messageText, to: payload.customerJid });

    const messageInfo = {
      customer: payload.customerId,
      to: payload.customerJid,
      uuid: uuidv1(),
      text: payload.text,
      isAgent: true,
      date: new Date(),
    };

    const messageObjPayload = keyBy([messageInfo], 'uuid');

    const customer = yield select(getCustomer(payload.customerId));
    customer.messages = [...customer.messages, messageInfo.uuid];
    const customerPayload = keyBy([customer], 'id');
    yield put(customerInfoReceived(customerPayload));

    yield put(agentMessageReceived(messageObjPayload))
  } catch (err) {
    yield put(agentMessageReceived(err))
  }
}

function* watchConnectClient() {
  yield takeLatest(connectClientAction, connectClient);
}

function* watchSendMessage() {
  yield takeLatest(sendMessageAction, sendMessage);
}

function* watchClientMessageObjReceived() {
  yield takeLatest(clientMessageObjReceivedAction, clientMessageObjReceived);
}

export default function* xmppSaga() {
  yield all([
    watchClientMessageObjReceived(),
    watchConnectClient(),
    watchSendMessage(),
  ]);
}



