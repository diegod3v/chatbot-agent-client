import { schema } from 'normalizr';

export const position = new schema.Entity('positions');

export const info = new schema.Entity('infos');

export const option = new schema.Entity('options')

export const node = new schema.Entity('nodes', {
  position: position,
  info: info,
  options: [option],
});

export const flowConfig = {
  nodes: [node],
}

export const message = new schema.Entity('messages')

export const customer = new schema.Entity('customers', {
  messages: [message],
});

export const chat = {
  customers: [customer],
}
