export const getFlowConfig = state => state.agent.flowConfig;
export const getNodes = state => state.agent.nodes;
export const getNode = (id) => state => state.agent.nodes[id];