export const getCustomers = state => state.chat.customers;
export const getCustomer = id => state => state.chat.customers[id];